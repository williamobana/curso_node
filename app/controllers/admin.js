module.exports.formulario_inclusao_noticia = function(application, req, res){
	res.render('admin/formulario_inclusao_noticia', {validacao : false, noticia : {}});
}

module.exports.salvar_noticia = function(application, req, res){
	
	var noticia = req.body;

	req.assert('titulo','Título é obrigatório').notEmpty();
	req.assert('resumo','Resumo é obrigatório').notEmpty();
	req.assert('resumo', 'Resumo deve conter entre 10 e 100 caracteres').len(10,100);
	req.assert('autor', 'Autor é obrigatório').notEmpty();
	req.assert('data_noticia', 'Data Noticia é obrigatória').notEmpty();

	var erros = req.validationErrors();

	if (erros){
		//console.log(erros);
		res.render('admin/formulario_inclusao_noticia', {validacao : erros ,  noticia : noticia});
		return;
	}

	var connection = application.config.dbConnection();

	var NoticiasDAO = new application.app.models.NoticiasDAO(connection);

	NoticiasDAO.salvarNoticia(noticia, function(error, result){

		res.redirect('/noticias');
	});
		//res.send(noticia)
}