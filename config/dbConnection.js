var mysql = require('mysql');

var mySqlConn = function(){
	console.log('Conexão estabelecida com o Banco de Dados')
	return  mysql.createConnection({
		host : 'localhost',
		user : 'root',
		password : '1234',
		database : 'portal_noticias'
	})
}

module.exports = function(){
	console.log('Autoload do Banco de Dados')
	return mySqlConn;

}